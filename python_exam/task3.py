from robot_control_class import RobotControl

class ExamControl:
    def __init__(self):
        self.rc = RobotControl()
        self.inf = self.rc.get_laser(360)

    def get_laser_readings(self):
        self.left=self.rc.get_laser(719)
        self.right=self.rc.get_laser(0)
        #print (self.left, self.right)
        return self.left, self.right

    def main(self):
        laser_values = self.get_laser_readings()
        while laser_values[0] != self.inf or laser_values[1] != self.inf:
            self.rc.move_straight() 
            laser_values = self.get_laser_readings()
            #print(laser_values)
        self.rc.stop_robot() 
#x = ExamControl()
#x.get_laser_readings()
#x.main()

