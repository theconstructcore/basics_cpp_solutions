from robot_control_class import RobotControl

rc=RobotControl()

res=rc.get_laser(360)

while res > 1:
    rc.move_straight()
    res=rc.get_laser(360)

rc.stop_robot()
rc.rotate(-80)