from robot_control_class import RobotControl
import time

robotcontrol = RobotControl()

front = robotcontrol.get_laser(360)
left = robotcontrol.get_laser(719)

with open('front.txt','w') as f:
    f.write(str(front))

with open('left.txt','w') as f:
    f.write(str(left))