from robot_control_class import RobotControl

def move():

    res = rc.get_laser(360)
    while res > 1:
        rc.move_straight()
        res = rc.get_laser(360)
    rc.stop_robot()

rc = RobotControl()
print (rc.get_laser(360))

move()
rc.rotate(-83)
move()
rc.rotate(-83.5)
move()
rc.rotate(93)
rc.move_straight_time("forward",1,6)