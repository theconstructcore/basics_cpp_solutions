cmake_minimum_required(VERSION 3.0.2)
project(unit_3_services)


find_package(catkin REQUIRED COMPONENTS
  roscpp
  my_custom_srv_msg_pkg
)



catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES unit_3_services
#  CATKIN_DEPENDS roscpp
#  DEPENDS system_lib
)

include_directories(
# include
  ${catkin_INCLUDE_DIRS}
)


add_executable(bb8_move_in_circle_service_server src/bb8_move_in_circle_service_server.cpp)
add_dependencies(bb8_move_in_circle_service_server ${bb8_move_in_circle_service_server_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(bb8_move_in_circle_service_server
   ${catkin_LIBRARIES}
 )

add_executable(bb8_move_in_circle_service_client src/bb8_move_in_circle_service_client.cpp)
add_dependencies(bb8_move_in_circle_service_client ${bb8_move_in_circle_service_client_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(bb8_move_in_circle_service_client
   ${catkin_LIBRARIES}
 )

add_executable(bb8_move_custom_service_server src/bb8_move_custom_service_server.cpp)
add_dependencies(bb8_move_custom_service_server ${bb8_move_custom_service_server_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(bb8_move_custom_service_server
   ${catkin_LIBRARIES}
 )


add_executable(bb8_move_custom_service_client src/bb8_move_custom_service_client.cpp)
add_dependencies(bb8_move_custom_service_client ${bb8_move_custom_service_client_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(bb8_move_custom_service_client
   ${catkin_LIBRARIES}
 )

add_executable(bb8_move_circle_class src/bb8_move_circle_class.cpp)
add_dependencies(bb8_move_circle_class ${bb8_move_circle_class_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(bb8_move_circle_class
   ${catkin_LIBRARIES}
 )
