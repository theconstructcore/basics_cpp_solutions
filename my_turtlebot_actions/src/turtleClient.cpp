#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <std_msgs/Empty.h>
#include <nav_msgs/Odometry.h> 
#include <math.h>
#include <my_turtlebot_actions/EscapeAction.h>


void doneCb(const actionlib::SimpleClientGoalState& state, //IDK IF ITS RIGHT im not even using but it is taking my goal and my result // SimpleClient
            const my_turtlebot_actions::EscapeResultConstPtr& result) //I DONT HAVE A GOAL SO I CANT PRINT ANYTHING OUT EITHER
{
  //ROS_INFO("[State Result]: %s", state.toString().c_str()); //dont rlly need this
  ROS_INFO("The Action has been completed");
  ROS_INFO("this some fun shi %f ", result->result_odom_array[1].pose.pose.position.x); //THIS IS THE RESULT
  //IMA TRY TO PRINT OUT RESULT SOMEHOW
}

// Called once when the goal becomes active
void activeCb() { ROS_INFO("Goal just went active"); } //COOL BEANS

void feedbackCb(const my_turtlebot_actions::EscapeFeedbackConstPtr& feedback){} //THERE IS NO FKING FEEDBACK LOLOSLOSLOSLOS



int main(int argc, char** argv)
{
  ros::init(argc, argv, "turtleClient"); // Initializes the action client node
  // Create the connection to the action server
  actionlib::SimpleActionClient<my_turtlebot_actions::EscapeAction> client("turtleAction", true);
  client.waitForServer(); // Waits until the action server is up and running 
  my_turtlebot_actions::EscapeGoal goal;
  client.sendGoal(goal, &doneCb, &activeCb, &feedbackCb); 
  //client.waitForResult();
  ros::Rate loop_rate(2);
  actionlib::SimpleClientGoalState state_result = client.getState(); //THIS IS JUST TO SEE IF THE ACTION IS ACTIVE BUT HOW DO I CHECK THIS 
  ROS_INFO("[State Result]: %s", state_result.toString().c_str());
  
  while ( state_result == actionlib::SimpleClientGoalState::ACTIVE || state_result == actionlib::SimpleClientGoalState::PENDING )
  {
    ROS_INFO("Doing Stuff while waiting for the Server to give a result...");
    loop_rate.sleep();
    state_result = client.getState();
    ROS_INFO("[State Result]: %s", state_result.toString().c_str());
  }
  
  return 0;
}

//THE ConstPtr is added at the end of the member I should figure out what it does