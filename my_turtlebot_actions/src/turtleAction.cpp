#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <std_msgs/Empty.h>
#include <nav_msgs/Odometry.h> 
#include <math.h>
#include <my_turtlebot_actions/EscapeAction.h>

class escapeTurtle {
    protected:
        int rate_hz;
        float time_limit; //duration
        bool action_active;
        float x;
        float y;
        bool finish;
        float constant;
        float hypt;
        
        
        ros::NodeHandle nh;
        nav_msgs::Odometry *odom_hold; // I need this shi to hold my nav_msgs/Odom I DONT THINK I DID THIS RIGHT
        ros::Subscriber sub_read;
        ros::Rate *rate_;
        actionlib::SimpleActionServer<my_turtlebot_actions::EscapeAction> as_; // i might wanna rename this later
        std::string action_name_; // i need to input this for the action name;
        my_turtlebot_actions::EscapeResult result_; //basically in the msg these r the thing im returning
        
        nav_msgs::Odometry latest_odometry;
    
    public:
    
        escapeTurtle(std::string name):
            as_(nh, name, boost::bind(&escapeTurtle::executeCB, this, _1), false), 
            action_name_(name) //this means that action_name_ = string name; 
        {
            ROS_INFO("Inside EscapeTurtle Action Init...");
            this->time_limit = 200; //this will be seconds of how long I will allow the robot to escape the maze
            this->rate_hz = 20;
            this->constant = 6; //this is magnitude I will use
            this->action_active = false;
            this->finish = false;
            
            sub_read = nh.subscribe<nav_msgs::Odometry>("/odom",1,&escapeTurtle::giveOdomMod,this);
            rate_= new ros::Rate(this->rate_hz);
    
            as_.start();
            ROS_INFO("Inside EscapeTurtle Action Init...END");
          
        }
        
        ~escapeTurtle(){
          delete [] odom_hold;
        }
        
        void waste_time(){
            rate_->sleep();
        }
        
        void executeCB(const my_turtlebot_actions::EscapeGoalConstPtr &gg){

            ROS_INFO("Action has been called...Time Starts Counting to get out of the maze");
            this->action_active = true;
            
            while (this->time_limit > 0 && this->finish == false ){ 
            
                if (as_.isPreemptRequested() || !ros::ok()){         // set the action state to preempted
                    ROS_INFO("%s: Preempted", action_name_.c_str());
                    as_.setPreempted();
                    break;
                } //if the action is preempted
                
                this->finish = this->are_we_out_of_maze();
                
                time_limit -= 1/(float)rate_hz; //this is changing time_limit to go to 0
                this->waste_time();
                ROS_INFO("the time left is %f", time_limit);
            }
            
            
            // If we finished because we got out of the maze, then its a success.
            if(this->finish) { 
                ROS_INFO("%s: Succeeded", action_name_.c_str());
                as_.setSucceeded(result_);
            }else
            {
                ROS_INFO("%s: Aborted", action_name_.c_str());
                //set the action state to aborted
                as_.setAborted(result_);
            }
            
            this->action_active = false;
        }
         
        void giveOdomMod(const nav_msgs::Odometry::ConstPtr &_msg){ //msg is a pointer 
            /***
             * We Fetch the latest odometry data and save it in a vector 
             * 
             * **/
         
            this->latest_odometry.header = _msg->header;
            this->latest_odometry.child_frame_id = _msg->child_frame_id;
            this->latest_odometry.pose = _msg->pose;
            
            if (this->action_active)
            {
                this->result_.result_odom_array.push_back(this->latest_odometry);
            }
            
            
        }
        
        bool are_we_out_of_maze()
        {
            bool result = false;
            
            // This is a security mesure to guarantee that the saved element in the vector is 
            // what we use for calculating.
            
            // We check if its empty otherwise cpp just crashes without any error message
            // [turtleAction-1] process has died [pid 4894, exit code -11, cmd /home/user/catkin_ws/devel/lib/my_turtlebot_actions/turtleAction __name:=turtleAction __log:=/home/user/.ros/log/3bfc0060-d1e7-11e8-8ee9-064b4381d178/turtleAction-1.log].
            // log file: /home/user/.ros/log/3bfc0060-d1e7-11e8-8ee9-064b4381d178/turtleAction-1*.log
            
            if (!this->result_.result_odom_array.empty())
            {
                nav_msgs::Odometry last_odom = this->result_.result_odom_array.back();
                float last_x = last_odom.pose.pose.position.x;
                float last_y = last_odom.pose.pose.position.y;
                
                float hypt = sqrt(pow(last_x,2)+pow(last_y,2));
                // Because the robot starts at the origin we dont have to save the init position
                // To then compare with the latest.
                if (hypt >= this->constant ){
                    ROS_WARN("SUCCESS Distance from origin %f >= %f", hypt, this->constant);
                    result = true;
                }else
                {
                    ROS_ERROR("NOTYET Distance from origin %f >= %f", hypt, this->constant);
                }
            }else
            {
                ROS_ERROR("Not data yet inside this->result_.result_odom_array");
            }
            
            
            return result;
        }
  
};

int main(int argc, char** argv){
    
  ros::init(argc, argv, "turtleAction");

  ROS_INFO("Starting Action...");
  escapeTurtle waft("turtleAction"); //this is the action naming
  ROS_INFO("Starting Action...END, now spining");
  ros::spin();
  
  return 0;
}